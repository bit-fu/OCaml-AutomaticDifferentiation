# Automatic Differentiation in OCaml

Inspired by [Automatic Differentiation in 38 lines of Haskell](https://gist.github.com/ttesmer/948df432cf46ec6db8c1e83ab59b1b21).

OCaml doesn't have Haskell's type classes, so it's somewhat less automatic, but you can still apply the `Dual` functor to a module that operates on an AST.
