(* =========================================================================== *
 *
 *    autodiff.ml
 *    ~~~~~~~~~~~
 *
 *    Automatic Differentiation experiment
 *
 *    Target language:    OCaml-4.12
 *
 *    Created 2022-09-18: Ulrich Singer
 *)


(* The `Float` module fulfills this signature, but
   one could also use a module operating on an AST. *)
module type Space_S =
sig

  type t

  val zero : t
  val one  : t

  val neg  : t -> t
  val add  : t -> t -> t
  val sub  : t -> t -> t
  val mul  : t -> t -> t
  val div  : t -> t -> t
  val pow  : t -> t -> t
  val log  : t -> t
  val exp  : t -> t
  val sin  : t -> t
  val cos  : t -> t
  val tan  : t -> t
  val sinh : t -> t
  val cosh : t -> t
  val tanh : t -> t

end   (* Space_S *)


type ('value, 'deriv) dual =
  D of ('value * 'deriv)
  [@@unboxed]


module Dual (Spc : Space_S) =
struct

  type value = Spc.t
  type deriv = Spc.t

  type t = (value, deriv) dual

  let _two = Spc.(add one one)

  (* Value of the variable being differentiated. *)
  let variable : value -> t
  = fun arg ->
    D (arg, Spc.one)

  (* Values whose derivative is zero. *)
  let constant : value -> t
  = fun arg ->
    D (arg, Spc.zero)

  (* F' = ∂F / ∂Var *)
  let diff : (t -> t) -> t -> deriv
  = fun fnc arg ->
    let D (_, y') = fnc arg in
    y'

  (* (-U)' = -(U') *)
  let neg : t -> t
  = fun (D (u, u')) ->
    D Spc.(neg u, neg u')

  (* (U + V)' = U' + V' *)
  let add : t -> t -> t
  = fun (D (u, u')) (D (v, v')) ->
    D Spc.(add u v, add u' v')

  (* (U - V)' = U' - V' *)
  let sub : t -> t -> t
  = fun (D (u, u')) (D (v, v')) ->
    D Spc.(sub u v, sub u' v')

  (* (U * V)' = U' V + V' U *)
  let mul : t -> t -> t
  = fun (D (u, u')) (D (v, v')) ->
    D Spc.(mul u v, add (mul u' v) (mul v' u))

  (* (U / V)' = (U' V - V' U) / V^2 *)
  let div : t -> t -> t
  = fun (D (u, u')) (D (v, v')) ->
    D Spc.(div u v, div (sub (mul u' v) (mul v' u)) (pow v _two))

  (* (U ^ V)' = V * U^(V-1) * U' + ln U * U^V * V' *)
  let pow : t -> t -> t
  = fun (D (u, u')) (D (v, v')) ->
    let u_up_v = Spc.pow u v in
    D Spc.(u_up_v, add (mul (mul (pow u (sub v one)) v) u') (mul (mul (log u) u_up_v) v'))

  (* (ln U)' = 1 / U * U' *)
  let log : t -> t
  = fun (D (u, u')) ->
    D Spc.(log u, div u' u)

  (* (exp U)' = exp U * U' *)
  let exp : t -> t
  = fun (D (u, u')) ->
    let exp_u = Spc.exp u in
    D Spc.(exp_u, mul exp_u u')

  (* (sin U)' = cos U * U' *)
  let sin : t -> t
  = fun (D (u, u')) ->
    D Spc.(sin u, mul (cos u) u')

  (* (cos U)' = -sin U * U' *)
  let cos : t -> t
  = fun (D (u, u')) ->
    D Spc.(cos u, mul (neg (sin u)) u')

  (* (tan U)' = 1 / (cos U)^2 * U' *)
  let tan : t -> t
  = fun (D (u, u')) ->
    D Spc.(tan u, div u' (pow (cos u) _two))

  (* (sinh U)' = cosh U * U' *)
  let sinh : t -> t
  = fun (D (u, u')) ->
    D Spc.(sinh u, mul (cosh u) u')

  (* (cosh U)' = sinh U * U' *)
  let cosh : t -> t
  = fun (D (u, u')) ->
    D Spc.(cosh u, mul (sinh u) u')

  (* (tanh U)' = 1 / (cosh U)^2 * U' *)
  let tanh : t -> t
  = fun (D (u, u')) ->
    D Spc.(tanh u, div u' (pow (cosh u) _two))

end   (* Dual () *)


(* Instantiating `Dual` for the `Float` module. *)
module DF = Dual (Float)


(* ~ autodiff.ml ~ *)
